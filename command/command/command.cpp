﻿// command.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"
#include <iostream>

using namespace std;

class Command;
class Receiver;

class Director
{
public:
	void SetCommand(Command* pCmd) { pCommand = pCmd; }
	void execute() { pCommand->execute(); }

private:
	Command* pCommand;
};

class Command
{
public:
	virtual void execute() = 0;
};

class filemakeCommand : public Command
{
public:
	void SetReceiver(Receiver* r) { pReceiver = r; }
	void execute() { pReceiver->execute(); }

private:
	Receiver* pReceiver;
};

class filesaveCommand : public Command
{
public:
	void SetReceiver(Receiver* r) { pReceiver = r; }
	void execute() { pReceiver->execute(); }

private:
	Receiver* pReceiver;
};

class filedeleteCommand : public Command
{
public:
	void SetReceiver(Receiver* r) { pReceiver = r; }
	void execute() { pReceiver->execute(); }

private:
	Receiver* pReceiver;
};

class Receiver
{
public:
	virtual void execute() = 0;
};

class filemake : public Receiver
{
public:
	virtual void execute() { cout << " 파일 생성 " << endl;  }
};

class filesave : public Receiver
{
public:
	virtual void execute() { cout << " 파일 저장 " << endl; }
};

class filedelete : public Receiver
{
public:
	virtual void execute() { cout << " 파일 삭제 " << endl; }
};


int main()
{
	Director* mDirector;
	filemake* mfilemake;
	filemakeCommand* mfilemakecommand;

	mDirector = new Director;
	mfilemake = new filemake;
	mfilemakecommand = new filemakeCommand;

	// 명령수행 오브젝트 지정
	mfilemakecommand->SetReceiver(mfilemake);

	// 명령지정
	mDirector->SetCommand(mfilemakecommand);


	// 실행
	mDirector->execute();
}
