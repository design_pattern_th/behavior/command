Command README.md
===

![이미지](image/command_image.PNG)<br>

명령을 내리는 부분(Director)

명령을 수행하는 부분(Receiver)

명령 그 자체(Command)

를 모두 나누고 싶을때 사용한다.

```c

int main()
{
	Director* mDirector;
	filemake* mfilemake;
	filemakeCommand* mfilemakecommand;

	mDirector = new Director;
	mfilemake = new filemake;
	mfilemakecommand = new filemakeCommand;

	// 명령수행 오브젝트 지정
	mfilemakecommand->SetReceiver(mfilemake);

	// 명령지정
	mDirector->SetCommand(mfilemakecommand);


	// 실행
	mDirector->execute();
}


```